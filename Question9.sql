select
y.category_name,
sum(x.item_price) as total_price
from
item x

inner join
item_category y
on
x.category_id=y.category_id

group by
y.category_name

order by
total_price desc;
